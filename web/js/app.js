var bookApp = angular.module('bookApp', ['ngRoute', ]);
//bookApp.config(function($interpolateProvider) {
//    $interpolateProvider.startSymbol(['[[','{{']);
//    $interpolateProvider.endSymbol([']]','}}']);
//});
bookApp.directive('loading',function($http,$rootScope){
        return {
            restrict:'E',
            link :function(scope,elem,attrs,ctrl){
                $rootScope.inProgress=true;
                scope.isLoading=function(){
                    return $http.pendingRequests.length>0;
                };
                scope.$watch(scope.isLoading,function(loading){
                    $rootScope.inProgress=loading;
                });
            },
            template: '<img ng-show="inProgress" src="img/ajax-loader.gif" alt="Loading..." title="Loading...">'
        };
    })



var path = 'js/app/';
var spanPage = '#main .crumbs .page';

// настроим маршруты
bookApp.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : path+'pages/home.html',
            controller  : 'mainController'
        })
        .when('/add', {
            templateUrl : path+'pages/form.html',
            controller  : 'formController'
        })
        .when('/:bookId', {
            templateUrl : path+'pages/item.html',
            controller  : 'itemController'
        })
        .when('/:bookId/edit', {
            templateUrl : path+'pages/form.html',
            controller  : 'formController'
        });
});

bookApp.controller('mainController', ['$http', '$scope',  function($http, $scope) {
    var page = 1;
    $scope.sort = 'none';

    $scope.page = page;
    var getBooks = function(page){
        angular.element(document.querySelector(spanPage)).html('');
        var path = '/api/books/';
        var filter = ['page='+page];
        var sort = $scope.sort;
        if(sort != 'none'){
            filter.push('sort='+sort);
        }
        var search = $scope.search;
        if(search){
            filter.push('search='+search);
        }
        if(filter.length)path = path + '?' + filter.join('&');
        //console.log(path);
        $http.get(path).then(function(response) {
            var books = response.data.books;
            if(books.length > 0){
                books.forEach(function(item) {
                    if(item.cover == '')item.cover = 'img/book_no_cover.png';
                });
            }
            var kolpages = response.data.kolpages;
            var pages = [];
            if(kolpages > 1){
                for(i=1; i <= kolpages; i++){
                    pages.push(i);
                }
            }else{
                pages.push(1);
            }

            $scope.kolpages = kolpages;
            $scope.pages = pages;
            $scope.books = books;


        });
    }
    $scope.bookSort = function(){
        getBooks(page);
    }
    $scope.bookSearch = function(){
        //console.log($scope.search);
        getBooks(page);
    }
    $scope.toPage = function(page){
        $scope.page = page;
        getBooks(page);
    }
    getBooks(page);
}]);

bookApp.controller('itemController', ['$routeParams', '$http', '$scope',  function($routeParams, $http, $scope) {
    var id = $routeParams.bookId;
    var bookName;
    $http.get('/api/books/'+ id+'/').then(function(response) {
        var book = response.data;
        if (book.cover == '')book.cover = 'img/book_no_cover.png';
        $scope.book = book;
        bookName = response.data.title;
        angular.element(document.querySelector(spanPage)).html(bookName)
    });

    $scope.delete = function(){ //удаление записи
         if(confirm('Подтвердите удаление записи')){
             $http.delete('/api/books/'+id+'/')
                 .success(function (data) {
                     //console.log('data:', data);
                     window.location = '/';
                 })
                 .error(function (data) {
                     console.log('Error:', data);
                 });
         }
    }
}]);

bookApp.controller('formController', ['$routeParams', '$http', '$scope',  function($routeParams, $http, $scope) { // форма
    var id = (!$routeParams.bookId) ? 0 : parseInt($routeParams.bookId);
    var charsLimits = { //ограниченмя длины полей
        'title' : 150,
        'author' : 100,
        'descr' : 2000
    }

    $scope.limits = charsLimits;
    $scope.messages = {
        'empty_field' : 'Поле не заполнено',
        'max_length' : 'Превышено максимальное количество символов'

    }
    $scope.hideMessage = true;

    if(id != 0){
        $http.get('/api/books/'+id+'/').then(function(response) {
            var book = response.data;
            $scope.id = book.id;
            $scope.title = book.title;
            $scope.author = book.author;
            $scope.year = book.year;
            $scope.descr = book.descr;
            //console.log(self.book);
            angular.element(document.querySelector(spanPage)).html('Редактирование книги: '+ book.title);
        });

    }else{
        var action = 'add';
    }



    $scope.submitForm = function(){


        if ($scope.bookForm.$valid) {
            var book = {
                id : $scope.id,
                title : $scope.title,
                author : $scope.author,
                year : $scope.year,
                descr : $scope.descr

            }
            //var fm = new FormData();
            //fm.append('id',book.id);
            //fm.append('title',book.title);
            //fm.append('author',book.author);
            //fm.append('year',book.year);
            //fm.append('descr',book.descr);
            //fm.append('test',111);

            //console.log($scope.bookForm);
            //console.log(book);
            //если id 0 - post books , else put books/id/
            if(action == 'add'){
                $http.post('/api/books/', book)
                    .success(function (data) {
                        console.log('data:', data);
                        if(data.id){
                            window.location = '/#'+data.id;
                        }
                    })
                    .error(function (data) {
                        console.log('Error:', data);
                    });
            }else{
                $http.put('/api/books/'+ book.id +'/', book)
                    .success(function (data) {
                        console.log('data:', data);
                        $scope.hideMessage = false;
                        $scope.resultMessage = 'Информация сохранена';
                    })
                    .error(function (data) {
                        $scope.hideMessage = false;
                        $scope.resultMessage = 'Информация не сохранена';
                        console.log('Error:', data);
                    });
            }
        }else console.log('form is not valid');
    }

}]);