<?php

namespace Orbit\OrbitBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Orbit\OrbitBundle\Entity\Book;
use Orbit\OrbitBundle\Form\BookType;


class MainController extends Controller
{
    public function indexAction()
    {
        return $this->render('BookBundle:Main:index.html.twig');
    }
//    public function detailAction()
//    {
//        return $this->render('BookBundle:Main:detail.html.twig');
//    }

//    public function addEditAction($id = 0)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        if ( $id ){
//            $book = $em->getRepository('BookBundle:Book')->find($id);
//        } else {
//            $book = new book();
//        }
//
//
//
//
//
//
//        $form = $this->createForm(BookType::class, $book);
//        return $this->render('BookBundle:Main:addedit.html.twig', array(
//            'form' => $form->createView()
//        ));
//    }

}
