<?php

namespace Orbit\OrbitBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Orbit\OrbitBundle\Entity\Book;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;


class ApiController extends FOSRestController
{
    public function detailAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $result = $em->getRepository('BookBundle:Book')->find($id);
        if ($result === null) {
            return new View("Book not found", Response::HTTP_NOT_FOUND);
        }
        return $result;
    }
    public function listAction(Request $request)
    {
        $limit = 4;

        $json = array(
            'total' => 0,
            'books' => null,
            'kolpages' => 1
        );

        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;
        $start = ($page != 1) ? (($page-1)*$limit) : 0;
        $sort = $request->get('sort');
        $search = $request->get('search');

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder('b');
        $total = $qb->select('COUNT(b.id)')->from('BookBundle:Book', 'b')->getQuery()->getSingleScalarResult();
        $kolpages = ceil($total/$limit);

        $qu = $qb->select('b');
        if(!empty($search)){
            $limit = 0;
            $kolpages = 1;
            $qu->where('b.title LIKE :search or b.author LIKE :search');
            $qu->setParameter('search' , '%'.$search.'%');
        }
        if(!empty($sort))$qu->addOrderBy('b.'.$sort , 'ASC');


//
        if($limit)$qu->setMaxResults($limit);
        if($start)$qu->setFirstResult($start);


        $books = $qu->getQuery()->getResult();

        if ($books === null) {
            return new View([], Response::HTTP_OK);
        }else{
            $json['books'] = $books;
            $json['total'] = $total;
            $json['kolpages'] = $kolpages;
            return new View($json,  Response::HTTP_OK);
        }

    }


    public function saveAction(Request $request){
        $isValid = true;
        $fieldlist = array(
            'id',
            'title',
            'author',
            'year',
            'descr'
        );

        $fields = array();
        foreach($fieldlist as $field){
            $fields[$field] = $request->get($field);
        }


        $action = (empty($fields['id'])) ? 'add' : 'edit';

        if($isValid){
            $em = $this->getDoctrine()->getManager();

            if($action == 'add'){
                $book = new Book();
            }else{

                $book = $em->getRepository('BookBundle:Book')->find($fields['id']);
                if(empty($book))return new View("Book not found", Response::HTTP_NOT_FOUND);
            }

            $book->setTitle($fields['title']);
            $book->setAuthor($fields['author']);
            $book->setYear($fields['year']);
            $book->setDescr($fields['descr']);
            $book->setCover('');
            $em->persist($book);
            $em->flush();
        }

        if($action == 'add'){
            $json = array('id' => $book->getId());
            return new View($json,  Response::HTTP_CREATED);
        }else{
            $json = array($fields);
            return new View($json,  Response::HTTP_OK);
        }

    }

    public function deleteAction($id){
        $em = $this->getDoctrine()->getManager();
        $book = $this->getDoctrine()->getRepository('BookBundle:Book')->find($id);
        if (empty($book)) {
            return new View("Book not found", Response::HTTP_NOT_FOUND);
        }
        else {
            $em->remove($book);
            $em->flush();
        }
        return new View("deleted successfully", Response::HTTP_NO_CONTENT);
    }
}
