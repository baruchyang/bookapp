<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.01.2017
 * Time: 2:47
 */
namespace Orbit\OrbitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        var_dump($options);
        $builder->add('title', TextType::class, array(
            'label' => 'Название',
            'attr' => array(
                'ng-model' => 'book.title',
            ))
        );//->setAttribute('ng-model' , 'book.title');
        $builder->add('author', TextType::class, array(
            'label' => 'Автор',
                'attr' => array(
                    'ng-model' => 'book.author',
                )
            ));
        $builder->add('year', TextType::class, array(
            'label' => 'Год издания','attr' => array(
                'ng-model' => 'book.year',
            )
        ));
        $builder->add('descr', TextareaType::class, array(
            'label' => 'Описание',
            'attr' => array(
                'ng-model' => 'book.descr',
            ))
        );
        $builder->add('id', HiddenType::class,array(
                'attr' => array(
                    'ng-model' => 'book.id'
                ))
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'book';
    }
}