<?php
/**
 * Created by PhpStorm.
 * User: Snatch
 * Date: 12.01.17
 * Time: 22:50
 */
// src/Blogger/BlogBundle/DataFixtures/ORM/BlogFixtures.php

namespace Orbit\OrbitBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Orbit\OrbitBundle\Entity\Book;

class BookFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $book1 = new Book();
        $book1->setTitle('Волшебник изумрудного города');
        $book1->setAuthor('Волков А.М.');
        $book1->setDescr('Сказочная повесть "Волшебник Изумрудного города" является переработкой сказки американского писателя Ф. Баума, Она рассказывает об удивительных приключениях девочки Элли и ее друзей в Волшебной стране');
        $book1->setYear(2016);
        $book1->setCover('');
        $manager->persist($book1);

        $book2 = new Book();
        $book2->setTitle('Чужак');
        $book2->setAuthor('Макс Фрай');
        $book2->setDescr('');
        $book2->setYear(2015);
        $book2->setCover('');
        $manager->persist($book2);

        $book3 = new Book();
        $book3->setTitle('Волонтеры вечности');
        $book3->setAuthor('Макс Фрай');
        $book3->setDescr('');
        $book3->setYear(2015);
        $book3->setCover('');
        $manager->persist($book3);

        $book4 = new Book();
        $book4->setTitle('Черновик');
        $book4->setAuthor('Сергей Лукьяненко');
        $book4->setDescr('');
        $book4->setYear(2014);
        $book4->setCover('');
        $manager->persist($book4);

        $book5 = new Book();
        $book5->setTitle('Чистовик');
        $book5->setAuthor('Сергей Лукьяненко');
        $book5->setDescr('');
        $book5->setYear(2015);
        $book5->setCover('');
        $manager->persist($book5);

        $manager->flush();
    }

}